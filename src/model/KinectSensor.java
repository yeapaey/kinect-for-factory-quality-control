/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package model;

import controller.FrameProcessor;
import edu.ufl.digitalworlds.j4k.DepthMap;
import edu.ufl.digitalworlds.j4k.J4KSDK;
import view.App;

/**
 * Singleton Design Pattern.
 */
public final class KinectSensor extends J4KSDK {
    
    public static final int SMOOTH_FACTOR = 5;
    private static KinectSensor INSTANCE;
    private FrameProcessor processor;
    private boolean snapshot = false;
    private boolean depthDone = false;
    private boolean colorDone = false;
    
    /**
     * Kinect INSTANCE private constructor.
     * @param kinect_type The type of the INSTANCE.
     * @param colorContent The Color window to drawArea on.
     * @param depthContent The Depth window to drawArea on.
     */
    private KinectSensor(byte kinect_type) {
        super(kinect_type);
        processor = FrameProcessor.getInstance();
    }
    
    
    /**
     * Static model.
     * @return the single instance of the model.
     */
    public static KinectSensor getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new KinectSensor(J4KSDK.MICROSOFT_KINECT_2);
        }
        return INSTANCE;
    }
    
    
    /**
     * Prints the Kinect INSTANCE's details.
     */
    public void printDetails() {
        System.out.println("----- Kinect Device Information -----");
        System.out.println("Device Type: " + getDeviceType());
        System.out.println("Color Resolution: " + getColorWidth() + "x" + getColorHeight());
        System.out.println("Depth Resolution: " + getDepthWidth() + "x" + getDepthHeight());
    }
    
    
    @Override
    public void onColorFrameEvent(byte[] color_data) {
        if (!depthDone) {
            processor.setLatestColorData(color_data);
            if (snapshot) {
                depthDone = true;
            }
        }
    }
    
    @Override
    public void onDepthFrameEvent(short[] depth_frame, byte[] player_index, float[] XYZ, float[] UV) {
        if (!colorDone) {
            DepthMap map = new DepthMap(App.DEPTH_WIDTH, App.DEPTH_HEIGHT, XYZ);
            map.setUV(UV);
            processor.setLatestDepthMap(map);
            if (snapshot) {
                colorDone = true;
            }
        }
    }
    
    @Override
    public void onSkeletonFrameEvent(boolean[] arg0, float[] arg1, float[] arg2,
            byte[] arg3) {
        //Unused.
    }

    public void setSnapshot(boolean isSnapshot) {snapshot = isSnapshot;}

}