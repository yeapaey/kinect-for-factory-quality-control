/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package model;

import java.awt.Color;

/**
 * Bottle model.
 */
public class Bottle {

    public final static int WIDTH = 16;
    public final static int HEIGHT = 30;
    private int minX, maxX, minY, maxY;
    private Color genericColor, realColor;
    
    public Bottle() {

    }

    public Bottle(int minX, int maxX, int minY, int maxY) {
        this.minX = minX;
        this.maxX = maxX;
        this.minY = minY;
        this.maxY = maxY;
    }
    
    public void setGenericColor(Color genericColor) {
        this.genericColor = genericColor;
    }
    
    public void setRealColor(Color realColor) {
        this.realColor = realColor;
    }
    
    public void setWidth(int minX, int maxX) {
        this.minX = minX;
        this.maxX = maxX;
    }

    public void setHeight(int minY, int maxY) {
        this.minY = minY;
        this.maxY = maxY;
    }
    
    public Color getGenericColor() {
        return genericColor;
    }
    
    public Color getRealColor() {
        return realColor;
    }
    
    public int getWidth() {
        return maxX-minX;
    }
    
    public int getHeight() {
        return maxY-minY;
    }
    
    public int getCenterX() {
        return maxX-(WIDTH/2);
    }
    
    public int getCenterY() {
        return maxY-(getHeight()/2);
    }
    
    public int getStartX() {
        return minX;
    }
    
    public int getEndX() {
        return maxX;
    }
    
    public int getStartY() {
        return minY;
    }
    
    public int getEndY() {
        return maxY;
    }
    
    /**
     * Checks if a bottle has a valid width.
     * @return The result of the check.
     */
    public boolean validWidth() {
        if(getWidth() > 12 && getWidth() < 18) {
            return true;
        }
        return false;
    }
    
    /**
     * Checks if a bottle has a valid height.
     * @return The result of the check.
     */
    public boolean validHeight() {
        if(getHeight() > 50 && getHeight() < 60) {
            return true;
        }
        return false;
    }
}