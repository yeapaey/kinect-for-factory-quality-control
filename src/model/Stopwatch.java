/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package model;

/*
 * VERY basic class for benchmarking. Just call start() to start and end() to print the time elapsed.
 * NOTE: Every call to start() will restart the counter so counters cannot overlap
 */
public class Stopwatch {
    private Stopwatch(){}

    private static long START = 0;

    public static void start() {
        START = System.currentTimeMillis();
    }
    public static void end() {
        System.out.println((System.currentTimeMillis() - START) / 1000.0 + " seconds");
        START = 0;
    }
}