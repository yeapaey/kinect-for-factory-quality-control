/*
 * Authored by Alex Yeap 2016
 * LICENSED under Apache 2.0 (http://directory.fsf.org/wiki/License:Apache2.0)
 */

package model

import BeSpaceDCore._

class ImportExport {

    // Expects array of format { X, Y, Z, X, Y, ...., Z }
    // Converts to BIGAND(Occupy3DPoint)
    def import3DSpace(data: Array[Int]): Invariant = {
        val points = pointsToOccupy3DPoints(data)
        return BIGAND(points)
    }

    // Same as import3DSpace but adds time implication for given TimePoint
    def import3DSpaceTime[T](data: Array[Int], timePoint: TimePoint[T]): Invariant = {
        return IMPLIES(timePoint, import3DSpace(data))
    }


    // TODO update this to use applyToConjunctionTermList???
    // Creates an Occupy3DBox determined by the min and max X, Y and Z values
    def inferOccupy3DBox(data: Array[Int]): Occupy3DBox = {
        val points = pointsToOccupy3DPoints(data)
        val maxXYZ: Occupy3DPoint = points.reduce((a,b) => comparePoints((c,d) => math.max(c,d))(a,b))
        val minXYZ: Occupy3DPoint = points.reduce((a,b) => comparePoints((c,d) => math.min(c,d))(a,b))

        return Occupy3DBox(maxXYZ.x,maxXYZ.y, maxXYZ.z, minXYZ.x, minXYZ.y, minXYZ.z)
    }


    // Accepts BIGAND (or nested BIGAND) of Occupy3DPoints and converts to flat Array[Int] (ie. java int[])
    // Convert this to use flatten???
    def export3DPoints(inv: Invariant): Array[Int] = {
        def splitTuples(in: List[(Int,Int,Int)]): List[Int] = {
            def iter(in: List[(Int, Int, Int)], out: List[Int]): List[Int] = {
                in match {
                    case Nil => out
                    case ((a,b,c)::tail) => iter(tail, a :: b :: c :: out)
                }

            }
            iter(in, Nil)
        }

        def extract3DPoints(inv: Invariant, lst: List[(Int,Int,Int)]): List[(Int,Int,Int)]  = {
            inv match {
                case BIGAND (Occupy3DPoint(xp,yp,zp)::xs) => extract3DPoints (BIGAND(xs), (xp,yp,zp) :: lst)
                case BIGAND (x::xs) => extract3DPoints (BIGAND(xs), extract3DPoints (x, lst)) // search nested BIGAND
                case _ => lst
            }
        }

        val points: List[(Int,Int,Int)] = extract3DPoints(inv, Nil)
        return splitTuples(points).toArray
    }

    // creates a new Occupy3DPoint from operations performed on equivalent [x|y|z] from two Occupy3DPoints
    def comparePoints(f: (Int,Int) => Int)(a: Occupy3DPoint, b: Occupy3DPoint): Occupy3DPoint = {
        val x = f(a.x, b.x)
        val y = f(a.y, b.y)
        val z = f(a.z, b.z)
        return new Occupy3DPoint(x,y,z)
    }

    // Expects a Seq of format { X, Y, Z, X, Y, Z, X ... , Z }
    def pointsToOccupy3DPoints(data: Array[Int]): List[Occupy3DPoint] = {
        val points: List[Occupy3DPoint] =
            for (i <- data.indices.toList if i % 3 == 2) yield {
                new Occupy3DPoint (data (i - 2), data (i - 1), data (i) )
            }
        return points
    }


    // converts array of floats to array of ints, scaled by order of magnitude
    // Needed because Occupy3DPoint etc. requires Int but Kinect outputs metres as floats
    // This may not be safe, it assumes that a -Infinity value for X will also result in -Infinity for Y and Z
    def floatsToInts(floats: Array[Float], order: Int): Array[Int] = {
        for (i <- floats.indices.toArray; if floats(i) != Float.NegativeInfinity) yield {
                (floats(i) * Math.pow(10, order)).toInt
        }
    }

}
