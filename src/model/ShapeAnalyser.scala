/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package model

class ShapeAnalyser {

    def analyse(linePoints: Array[Float], width: Int, height: Int): Array[Double] = {
        val points: List[List[Float]] = linePoints.toList.grouped(3).toList;
        var results: List[Double] = Nil
        for (i <- points.indices.slice(1, points.indices.length)) {
            val first = points(i - 1)
            val second = points(i)

            val x1 = first(0)
            val y1 = first(1)
            val z1 = first(2)
            val x2 = second(0)
            val y2 = second(1)
            val z2 = second(2)

            val hypot: Double = Math.hypot(x2-x1, z2-z1)
            val theta: Double = Math.toDegrees(Math.asin((z2 - z1) / hypot))

            results =  theta :: results
        }

        return results.reverse.toArray
    }
}
