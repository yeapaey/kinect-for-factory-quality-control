/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package view;

import javax.swing.JFrame;
import controller.FrameProcessor;
import controller.KinectSimulator;
import edu.ufl.digitalworlds.j4k.J4KSDK;
import model.KinectSensor;

public class App {
    
    public static final int DEPTH_WIDTH = 512;
    public static final int DEPTH_HEIGHT = 424;
    public static final int COLOUR_WIDTH = 1920;
    public static final int COLOUR_HEIGHT = 1080;
    private static JFrame colorWindow;
    private static JFrame depthWindow;
    private static ColorPanel colorContent;
    private static DepthPanel depthContent;
    private KinectSensor sensor;
    private FrameProcessor processor;
    private boolean useColor = false;
    private boolean useDepth = false;
    private boolean simulate = false;

    
    public App() {

        sensor = KinectSensor.getInstance();
        /* Convenience for activating/deactivating windows */
        useColor = true;
        useDepth = true;
        simulate = true;
//        sensor.setSnapshot(true);


        sensor.printDetails();
        processor = FrameProcessor.getInstance();

        if (useDepth) {
            initDepthWindow();
            processor.addObserver(depthContent);
        }
        
        if (useColor) {
            initColorWindow();
            processor.addObserver(colorContent);
        }
        
        if (simulate) {
            KinectSimulator.simulateKinect("res/");
        }
        else {
            sensor.start(J4KSDK.COLOR | J4KSDK.DEPTH | J4KSDK.XYZ | J4KSDK.UV);
        }
    }
    
    private void initDepthWindow() {
        depthContent = new DepthPanel();
        depthWindow = new JFrame("DEPTH");
        depthWindow.setContentPane(depthContent);
        depthWindow.setSize(527,462);
        depthWindow.setLocation(120,120);
        depthWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        depthWindow.setVisible(true);
    }
    
    private void initColorWindow() {
        colorContent = new ColorPanel();
        colorWindow = new JFrame("COLOR");
        colorWindow.setContentPane(colorContent);
        colorWindow.setSize(1920,1119);
        colorWindow.setLocation(0,0);
        colorWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        colorWindow.setVisible(true);
    }
    
    public static void main(String[] args) {
        new App();
    }
}