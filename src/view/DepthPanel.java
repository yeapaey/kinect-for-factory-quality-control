/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package view;

import edu.ufl.digitalworlds.j4k.DepthMap;
import model.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import controller.ArrayUtilities;
import controller.BottleScanner;
import controller.FrameProcessor;

/**
 * Observer Design Pattern.
 */
@SuppressWarnings("serial")
public class DepthPanel extends AbstractDisplayPanel implements Observer {
    private int minDepth = 40; // 40cm
    private static int DEFAULT_DRAW_DEPTH = 8000;
    
    
    /**
     * Draws a depth image on the window.
     * @param z The depth values of each pixel.
     * @param width The width of the image.
     * @param height The height of the image.
     * @param maxZ The maximum distance to draw to.
     */
    public void drawArea(int[] z, int width, int height, int maxZ, boolean color) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        /* Draw a depth image with color. */
        if(color) {
            FrameProcessor processor = FrameProcessor.getInstance();
            int[] colors = processor.getDepthToColorMap();

            for (int y = 0, i = 0; y < height; y++) {
                for (int x = 0; x < width; x++, i++) {
                    img.setRGB(x, y, colors[i]);
                }
            }
        }
        /* Draw a simple black/white depth image. */
        else {
            int black = 0x000000;
            int white = 0xFFFFFF;

            for (int y = 0, i = 0; y < height; y++) {
                for (int x = 0; x < width; x++, i++) {
                    if (z[i] >= minDepth && z[i] <= maxZ) {
                        img.setRGB(x, y, black);
                    } else {
                        img.setRGB(x, y, white);
                    }
                }
            }
        }
        drawer = getGraphics();
        drawer.drawImage(img, 0, 0, width, height, Color.WHITE, null);
        drawer.dispose();
    }
    
    
    /**
     * Highlights bottles in the depth image.
     * @param map The depth data for the whole image.
     * @param bottles The list of bottles to highlight.
     */
    public void highlightBottles(DepthMap map, List<Bottle> bottles) {
        
        float[][] xyz = ArrayUtilities.expandTo2D(map.realZ, map.getWidth(), map.getHeight());
        for(Bottle b: bottles) {
            drawer = getGraphics();
        
            /* Draw the bottle itself. */
            for(int i=b.getStartY(); i<b.getEndY(); i++) {
                for(int j=b.getStartX(); j<=b.getEndX(); j++) {
                    if(xyz[j][i] > BottleScanner._startingZ && xyz[j][i] < BottleScanner._offsetZ) {
                        drawer.setColor(b.getRealColor());
                        drawer.drawLine(j,i,j,i);
                    }
                }
            }
            
            /* Draw a bounding box around it. */
            drawBoundingBox(b.getStartX()-5, b.getStartY()-5, b.getEndX()+5, b.getEndY()+5, 2, Color.GREEN);
        }
        drawer.dispose();
    }
    

    /**
     * Updates the window's content.
     */
    @Override
    public void update(Observable o, Object arg) {
        FrameProcessor processor = FrameProcessor.getInstance();
        
        /* Update whole depth image. */
        drawArea(ArrayUtilities.floatsToInts(processor.getDepthMap().realZ, 3),
                App.DEPTH_WIDTH, App.DEPTH_HEIGHT, DEFAULT_DRAW_DEPTH, false);
        
        /* Highlight the bottles. */
        highlightBottles(processor.getDepthMap(), processor.getBottles());
    }
}