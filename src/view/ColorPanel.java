/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package view;

import model.Bottle;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.util.List;
import controller.FrameProcessor;

/**
 * Observer Design Pattern.
 */
@SuppressWarnings("serial")
public class ColorPanel extends AbstractDisplayPanel implements Observer {
    
    private FrameProcessor processor;
    
    public ColorPanel() {
        super();
        setBackground(Color.WHITE);
    }
    
    /**
     * Renders an image on the window from an array of color data.
     * @param color_data The color data.
     * @param width The width of the image.
     * @param height The height of the image.
     */
    public void renderImage(int[] color_data, int width, int height) {
        BufferedImage img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        
        for (int y = 0, counter = 0; y < height; y++) {
            for (int x = 0; x < width; x++, counter++) {
                img.setRGB(x, y, color_data[counter]);
            }
        }
        
        drawer = getGraphics();
        drawer.drawImage(img, 0, 0, width, height, Color.WHITE, null);
        drawer.dispose();
    }
    
    
    /**
     * Highlights bottles in the color image.
     * @param bottles The list of bottles to highlight.
     */
    public void highlightBottles(List<Bottle> bottles) {
        for (Bottle b : bottles) {
            Point topLeft = processor.getColorPointFromDepthPoint(b.getStartX(), b.getStartY());
            Point bottomRight = processor.getColorPointFromDepthPoint(b.getEndX(), b.getEndY());
            
            drawBoundingBox(topLeft.x-15, topLeft.y-15, bottomRight.x+15, bottomRight.y+15, 2, Color.GREEN);
        }
    }
    
    
    /**
     * Updates the window's content.
     */
    @Override
    public void update(Observable o, Object arg) {
        removeAll();
        processor = FrameProcessor.getInstance();
        
        /* Draw the whole color image. */
        renderImage(processor.getColorAsRGBInts(), App.COLOUR_WIDTH, App.COLOUR_HEIGHT);
        
        /* Highlight the bottles. */
        highlightBottles(processor.getBottles());
    }
}