/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package view;

import javax.swing.*;
import java.awt.*;


@SuppressWarnings("serial")
public class AbstractDisplayPanel extends JPanel {
    
    protected Graphics drawer;
    
    public AbstractDisplayPanel() {
        setBackground(Color.WHITE);
    }
    
    
    /**
     * Draws a bounding box on the window around an object of interest.
     * @param startX The starting point of the object on the X-axis.
     * @param startY The starting point of the object on the Y-axis.
     * @param endX The ending point of the object on the X-axis.
     * @param endY The ending point of the object on the Y-axis.
     * @param thickness The thickness of the bounding box.
     * @param color The color of the bounding box.
     */
    public void drawBoundingBox(int startX, int startY, int endX, int endY, int thickness, Color color) {
        drawer = getGraphics();
        
        if (color == null) color = Color.RED;
        drawer.setColor(color);
        
        for (int i = 0; i < thickness; ++i) {
            drawer.drawRect(startX-i, startY-i, (endX+i)-(startX-i), (endY+i)-(startY-i));
        }
        drawer.dispose();
    }
}