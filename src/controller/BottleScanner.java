/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

import edu.ufl.digitalworlds.j4k.DepthMap;
import model.Bottle;
import model.ShapeAnalyser;

import java.util.*;
import java.util.List;

public class BottleScanner {

    public final static float _startingZ = 0.85f;
    public final static float _offsetZ = 0.95f;
    private List<Bottle> bottles;

    public BottleScanner() {
        bottles = new ArrayList<>();
    }

    /**
     * Identifies bottles in a depth image. The depth imag is split into multiple sections.
     *
     * @param map The depth image.
     * @return An array of bottles that were identified.
     */
    public List<Bottle> scan(DepthMap map) {
        float[][] xyz = ArrayUtilities.expandTo2D(map.realZ, map.getWidth(), map.getHeight());
        int splitsX = 32;
        int splitsY = 14;
        int scanningX = 0;
        int scanningY = 0;
        int counter;

        bottles.clear();
        
        /* Try to identify a bottle in each section and highlight it in the image. */
        for (int s = 0; s < splitsX; s++) {
            int xIndex = scanningX + Bottle.WIDTH / 2;
            scanningY = 0;
            for (int y = 0; y < splitsY; y++) {
                int yIndex = scanningY + Bottle.HEIGHT / 2;

                if (validDepth(xyz[xIndex][yIndex])) {
                    /* Calculate the start and end points of the bottle on the X-axis. (width) */
                    int minX = 0, maxX = 0;
                    counter = 0;
                    /* Go left. */
                    while (validDepth(xyz[xIndex - counter][yIndex])) {
                        minX = xIndex - counter;
                        counter++;
                    }
                    counter = 0;
                    /* Go right. */
                    while (validDepth(xyz[xIndex + counter][yIndex])) {
                        maxX = xIndex + counter;
                        counter++;
                    }
                    
                    /* Calculate the start and end points of the bottle on the Y-axis. (height) */
                    int minY = 0, maxY = 0;
                    counter = 0;
                    /* Go up. */
                    while (validDepth(xyz[xIndex][yIndex - counter])) {
                        minY = yIndex - counter;
                        counter++;
                    }
                    counter = 0;
                    /* Go down. */
                    while (validDepth(xyz[xIndex][yIndex + counter])) {
                        maxY = yIndex + counter;
                        counter++;
                    }
                    
                    /* Check for duplicate. */
                    if (isDuplicate(minX, minY)) {
                        break;
                    }
                    
                    /* Set the bottles attributes and add it to the list if it appears valid. */
                    Bottle bottle = new Bottle();
                    bottle.setWidth(minX, maxX);
                    bottle.setHeight(minY, maxY);

                    if (bottle.validWidth() && bottle.validHeight()) {
                        bottles.add(bottle);
                    }
                }
                scanningY = scanningY + Bottle.HEIGHT;
            }
            scanningX = scanningX + Bottle.WIDTH;
        }
        return bottles;
    }


    /**
     * Checks if a pixel is within a valid depth.
     *
     * @param point The pixel to be checked.
     * @return The result of the check.
     */
    public boolean validDepth(float point) {
        if (point > _startingZ && point < _offsetZ) {
            return true;
        }
        return false;
    }

    /**
     * Checks if a bottle has already been processed.
     *
     * @param minX The starting point of the bottle in the X-axis.
     * @param minY The starting point of the bottle in the Y-axis.
     * @return The result of the check.
     */
    public boolean isDuplicate(int minX, int minY) {
        for (Bottle b : bottles) {
            if (Math.abs(b.getStartX() - minX) < Bottle.WIDTH / 2 && Math.abs(b.getStartY() - minY) < Bottle.HEIGHT / 2) {
                return true;
            }
        }
        return false;
    }


    /*
     * Note: just from eye-balling the theta values, it looks like curves are being thrown off by light,
     * such that some values will be incorrect (eg. decrease rather than increase) even though the general
     * trend is correct.  This can cause the bottle to be shorter it should be
     *
     * Need to do vertical shape to check for continuity
     */
    public boolean doShapeAnalysis2(Bottle bottle, DepthMap dMap) {
        List<float[]> rows = new ArrayList<>();
        int pointer;

        // Extract bottle xyz vals and split in to rows
        for (int h = bottle.getStartY(); h < bottle.getEndY(); h++) {
            float[] row = new float[bottle.getWidth() * 3];
            for (int w = bottle.getStartX(), i = 0; w < bottle.getEndX(); w++, i += 3) {
                pointer = w + (h * dMap.getWidth());
                row[i] = dMap.realX[pointer];
                row[i + 1] = dMap.realY[pointer];
                row[i + 2] = dMap.realZ[pointer];
            }
            rows.add(row);
        }

        ShapeAnalyser analyser = new ShapeAnalyser();
        int[] convexRows = new int[rows.size()];
        int numConvexRows = 0;
        int minPoints = 10;
        for (int rw = 0; rw < rows.size(); rw++) {  // For every row
//            float[] row = rows.get(rw);
            double[] thetas = analyser.analyse(rows.get(rw), bottle.getWidth(), bottle.getHeight());
            int[] len = new int[thetas.length];
            int currMax = 0;
            int maxIndices = -1;

            for (int el = 0; el < thetas.length - currMax; el++) { // For every element
                for (int i = el; i < thetas.length; i++) { // check if a curve starts there
                    double theta = thetas[i];
                    double prev = theta;
                    int points = 0;
                    boolean hasMidPoint = false;
                    boolean complete = false;

                    if (theta > -90 && theta <= -60) { // is start of curve
                        for (int k = el + 1; k < thetas.length; k++) {
                            double inTheta = thetas[k];
                            if (inTheta > prev && inTheta < 90) { // if in curve
                                points++;

                                if (inTheta > -20 && inTheta < 20) { // if inflection point. May need to tighten tolerances
                                    hasMidPoint = true;
                                }
                                else if (inTheta > 50 && hasMidPoint) { // acceptable end point (play with tolerances
                                    complete = true;
                                }
                            }
                            else { // not a valid curve value
                                break;
                            }

                        }


                    }
                    // should probably check if goes past midpoint
                    if (complete && points > currMax && points > minPoints) {
                        currMax = points;
                        maxIndices = el;
                    }

                }
//                len[el] = currMax;
            }
            if (maxIndices != -1) {
                convexRows[numConvexRows++] = rw;
            }
        }


        int threshold = Math.round(rows.size() * 0.6f); // % curves
        System.out.println(numConvexRows  * 1.0f / rows.size());
        return numConvexRows >= threshold;


        /* this code does height check. Try something else
         */
//        if (numConvexRows < 2) return false;  // at least 2 slices so you have something to compare
//        float[] tr = rows.get(convexRows[0]);
//        float[] br = rows.get(convexRows[numConvexRows - 1]);
//        float bottom = br[1];
//        float top = tr[1];
////        float top = rows.get(convexRows[0])[1]; // should be y value of first point in first convex slice
////        float bottom = rows.get(convexRows[counter - 1])[1]; // should be y value of first point in last convex slice
//
//        float height = Math.max(top, bottom) - Math.min(top, bottom);
//        if (height <= 0.1 && height >= 0.06) { // should be within 6-10 cm tall
//            return true;
//        } else {
//            return false;
//        }
    }
}