/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

import java.io.BufferedReader;
import java.io.FileReader;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import model.KinectSensor;

public class KinectSimulator {

	public static String XYZ = "split-depth_xyz.txt";
	public static String UV = "split-depth_uv.txt";
	public static String COLOR = "split-color.txt";
	
	
	/**
	 * Simulates a real Kinect sensor.
	 * @param dataBasepath The location of the sample files on the disk.
	 * @return Weather the simulation started successfully.
	 */
	public static boolean simulateKinect(String dataBasepath) {
		float[] xyz = csvToFloatArr(dataBasepath + XYZ);
		float[] uv = csvToFloatArr(dataBasepath + UV);
		float[] temp = csvToFloatArr(dataBasepath + COLOR);
		byte[] color_data = floatArrToByteArr(temp);
		
		if (xyz == null || uv == null || color_data == null ) {
			
			return false;
		}
		
        KinectSensor.getInstance().onColorFrameEvent(color_data);
        KinectSensor.getInstance().onDepthFrameEvent(null, null, xyz, uv);
		
		return true;
	}
	
	
	/**
	 * Converts a CSV to a float array.
	 * @param path The location of the CSV array on the disk.
	 * @return The new float array.
	 */
	public static float[] csvToFloatArr(String path) {
		BufferedReader reader = null;
		try
		{
			reader = new BufferedReader(new FileReader(path));
			List<String> lines = new ArrayList<>();
			String line;
			while ((line = reader.readLine()) != null) {
				lines.add(line);
			}
			reader.close();

			int lineLen = lines.get(0).split(",").length;
			float[] data = new float[lines.size() * lineLen];

			int counter = 0;

			for (String l : lines) {
				String[] vals = l.split(",");
				for (String v : vals) {
					data[counter] = Float.parseFloat(v);
					counter++;
				}
			}

			return data;
		} 
		catch (Exception e) {
			System.out.println("Exception: " + e.getMessage());
			return null;
		}
	}
	
	
	/**
	 * Converts a float array to a byte array.
	 * @param floats The float array to be converted.
	 * @return The new byte array.
	 */
	public static byte[] floatArrToByteArr(float[] floats) {
		if (floats == null ) return null;
		
		ByteBuffer buf = ByteBuffer.allocate(floats.length);

		for (int i = 0; i < floats.length; ++i) {
			buf.put((byte)(floats[i]));
		}
		
		return buf.array();
	}
}