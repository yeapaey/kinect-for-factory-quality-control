/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

import edu.ufl.digitalworlds.j4k.DepthMap;
import model.Bottle;
import model.KinectSensor;
import view.App;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

/**
 * Singleton Design Pattern. Observable.
 */
public class FrameProcessor extends Observable {
    
    private static FrameProcessor INSTANCE = null;
    private float[] _xyz;
    private float[] _uv;
    private byte[] _colorData;
    private int[] _colorDataAsInt;
    private DepthMap _depthMap;
    private int[] _depthToColorMap;
    private List<Bottle> _bottles;
    private boolean hasNewDepthFrame;
    private boolean hasNewColorFrame;
    private BottleScanner scanner;
    
    
    /**
     * INSTANCE private constructor.
     */
    private FrameProcessor() {
        hasNewColorFrame = false;
        hasNewDepthFrame = false;
        scanner = new BottleScanner();
    }
    
    
    /**
     * Static model.
     * @return the single instance of the model.
     */
    public static FrameProcessor getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new FrameProcessor();
        }
        return INSTANCE;
    }
    
    
    /**
     * Updates depth data with the latest.
     * @param map The latest depth map from the sensor.
     */
    public void setLatestDepthMap(DepthMap map) {
        _depthMap = map;
        if (map == null) {
            System.err.println("Map is null.");
        }
        hasNewDepthFrame = true;
        stateChanged();
    }
    
    
    /**
     * Updates color data with the latest.
     * @param colorData The latest color data from the sensor.
     */
    public void setLatestColorData(byte[] colorData) {
        _colorData = colorData;
        hasNewColorFrame = true;
        stateChanged();
    }
    
    
    /**
     * Processes the latest frame data.
     */
    private void process() {
        _depthMap.smooth(KinectSensor.SMOOTH_FACTOR);
        convertColorToRGBInts();
        buildDepthToColorMap();
        _bottles = scanner.scan(_depthMap);

        List<Bottle> confirmed = new ArrayList<>();
        for (Bottle b : _bottles) {
            if (scanner.doShapeAnalysis2(b, _depthMap)) {
                confirmed.add(b);
            }
        }
        _bottles = confirmed;
        identifyColor(_bottles);
    }
    
    public float[] getXYZ() {
        return _xyz;
    }
    
    public float[] getUV() {
        return _uv;
    }
    
    public byte[] getColorData() {
        return _colorData;
    }
    
    public DepthMap getDepthMap() {
        return _depthMap;
    }
    
    public List<Bottle> getBottles() {
        return _bottles;
    }
    
    public int getDepthWidth() {
        return _depthMap.getWidth();
    }
    
    public int getDepthHeight() {
        return _depthMap.getHeight();
    }
    
    public int getColorWidth() {
        return App.COLOUR_WIDTH;
    }
    
    public int getColorHeight() {
        return App.COLOUR_HEIGHT;
    }
    
    
    private void stateChanged() {
        if (hasNewColorFrame && hasNewDepthFrame) {
            process();
            setChanged();
            notifyObservers();
            hasNewColorFrame = false;
            hasNewDepthFrame = false;
        }
    }
    
    
    /**
     * Returns the location of a depth point in the color image.
     * @param depthX The X coordinate in the depth image.
     * @param depthY The Y coordinate in the depth image.
     * @return A point in the color image.
     */
    public Point getColorPointFromDepthPoint(int depthX, int depthY) {
        int i = depthX + depthY * getDepthWidth();
        float u = _depthMap.U[i];
        float v = _depthMap.V[i];
        
        /* If -infinity, search around for the next non -infinity value
         Should be close enough. Needs error checking to not exceed array bounds */
        int incr = 1;
        while (u == Float.NEGATIVE_INFINITY) {
            u = _depthMap.U[i + incr++];
        }
        
        incr = 1;
        while (v == Float.NEGATIVE_INFINITY) {
            v = _depthMap.V[i + incr++];
        }
        
        int colorX = Math.round(u * getColorWidth());
        int colorY = Math.round(v * getColorHeight());
        
        return new Point(colorX, colorY);
    }
    
    public int[] getColorAsRGBInts() {
        return _colorDataAsInt;
    }
    
    
    /**
     * Converts the byte[] color array to an equivalent int[] array. The resulting array
     * is three times smaller as the three RGB values are saved into a single value now.
     */
    private void convertColorToRGBInts() {
        int[] converted = new int[getColorHeight() * getColorWidth()];
        int b, g, r, color;
        int counter = 0;
        for (int y = 0; y < getColorHeight(); y++) {
            for (int x = 0; x < getColorWidth() && counter < _colorData.length; x++) {
                b = _colorData[counter] & 0xFF;
                g = _colorData[counter + 1] & 0xFF;
                r = _colorData[counter + 2] & 0xFF;

                color = (r << 16) | (g << 8) | b;
                converted[x + y * getColorWidth()] = color;
                //                img.setRGB(x, y, color);
                counter += 4;
            }
        }
        _colorDataAsInt = converted;
    }
    
    public int[] getDepthToColorMap() {
        return _depthToColorMap;
    }
    
    
    /**
     * Maps depth pixels to colors with the help of UV.
     */
    public void buildDepthToColorMap() {
        int colX, colY;
        int[] uvMap = new int[getDepthHeight() * getDepthWidth()];
        for (int y = 0, i = 0; y < getDepthHeight(); y++) {
            for (int x = 0; x < getDepthWidth(); x++, i++) {
                float u = _depthMap.U[x + y * getDepthWidth()];
                float v = _depthMap.V[x + y * getDepthWidth()];

                boolean nInfU = false;
                boolean nInfY = false;
                int front;

                /* Normalize UV floats to fit within the 0-1 UV range. */
                if (u == Float.NEGATIVE_INFINITY) {
                    nInfU = true;
                } else if (u < 0) {
                    front = (int) Math.ceil(u);
                    u = u + front + 1;
                } else if (u > 1) {
                    front = (int) Math.floor(u);
                    u = 1 - (u - front);
                }

                if (v == Float.NEGATIVE_INFINITY) {
                    nInfY = true;
                } else if (v < 0) {
                    front = (int) Math.ceil(v);
                    v = v + front + 1;
                } else if (v > 1) {
                    front = (int) Math.floor(v);
                    v = 1 - (v - front);
                }

                colX = (!nInfU ? Math.round(u * getColorWidth()) : x);
                colY = (!nInfY ? Math.round(v * getColorHeight()) : y);
                if (colX >= getColorWidth()) colX = getColorWidth() - 1;
                if (colY >= getColorHeight()) colY = getColorHeight() - 1;

                uvMap[i] = _colorDataAsInt[colX + colY * getColorWidth()];
            }
        }
        _depthToColorMap = uvMap;
    }
    
    
    /**
     * Identifies the colors for a list of bottles.
     * @param bottles The bottles whose color will be identified.
     */
    public void identifyColor(List<Bottle> bottles) {
        
        for(Bottle b: bottles) {

            /* Get the bottle's location in the color image. */
            Point topLeft = getColorPointFromDepthPoint(b.getStartX(), b.getStartY());
            Point bottomRight = getColorPointFromDepthPoint(b.getEndX(), b.getEndY());
            
            /* Calculate the average color. */
            int avgR = 0, avgG = 0, avgB = 0, counter = 0;
            for(int i=topLeft.x; i<bottomRight.x; i++) {
                for(int j=topLeft.y+(bottomRight.x-topLeft.x); j<bottomRight.y-(bottomRight.x-topLeft.x); j++) {
                    Color c = new Color(_colorDataAsInt[i + j*1920]);
                    avgR = avgR + c.getRed();
                    avgG = avgG + c.getGreen();
                    avgB = avgB + c.getBlue();
                    counter++;
                }
            }
            avgR = avgR/counter;
            avgG = avgG/counter;
            avgB = avgB/counter;
            
            /* Set the appropriate colors. */
            if(avgB > avgR && avgB > avgG) {
                b.setGenericColor(Color.BLUE);
            }
            else {
                b.setGenericColor(Color.BLACK);
            }
            b.setRealColor(new Color(avgR, avgG, avgB));
        }
    }
}