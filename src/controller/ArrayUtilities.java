/*
 * Kinect For Factory Quality Control - identifies bottles for FESTO system using depth data
 * Copyright (C) 2016  Alex Yeap, Vasileios Dimitrakopoulos, Nicholas Powell, Shane Basnayake
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package controller;

public class ArrayUtilities {
    
    /**
     * Expands a 1D array to a 2D array.
     * @param flat The 1D array to convert.
     * @param oneD The height of the 2D array.
     * @param twoD The width of the 2D array.
     * @return The new 2D array.
     */
    public static float[][] expandTo2D(float[] flat, int oneD, int twoD) {
        float[][] expanded = new float[oneD][twoD];

        int counter = 0;
        try
        {
            for(int i=0; i < twoD; i++) {
                for(int j=0; j < oneD; j++) {
                    expanded[j][i] = flat[counter];
                    counter++;
                }
            }
            
            return expanded;
        }
        catch (IndexOutOfBoundsException e) {
            System.out.println(e.getClass() + e.getMessage());
            return null;
        }
    }
    
    
    /**
     * Truncates floats to integers. Shift allows for decimal shift (ie. 10 ^ shift)
     * @param vals The floats to be converted.
     * @param shift The decimal shift.
     * @return The resulting integers.
     */
    public static int[] floatsToInts(float[] vals, int shift) {
        int scale = (int) Math.pow(10, shift);
        int[] converted = new int[vals.length];
        try
        {
            for (int i = 0; i < vals.length; i++) {
                converted[i] = (int) (vals[i] * scale);
            }
            
            return converted;
        }
        catch (Exception e) {
            System.out.println(e.getClass() + e.getMessage());
            return null;
        }
    }
}